<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Buku;

class BukuApiController extends Controller
{
    public function index()
    {
        return Buku::all();
    }

    public function store()
    {
        request()->validate([
            'judul' => 'required',
            'deskripsi' => 'required',
        ]);

        return Buku::create([
            'judul' => request('judul'),
            'deskripsi' => request('deskripsi'),
        ]);
    }

    public function update(Buku $buku)
    {
        request()->validate([
            'judul' => 'required',
            'deskripsi' => 'required',
        ]);

        $success = $buku->update([
            'judul' => request('judul'),
            'deskripsi' => request('deskripsi')
        ]);


        return [
            'success' => $success
        ];
    }

    public function delete(Buku $buku)
    {
        $success = $buku->delete();

        return [
            'success' => $success
        ];
    }
}
