<?php

use App\Models\Buku;
use App\Http\Controllers\BukuApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/bukus', [BukuApiController::class, 'index']);
Route::post('/bukus', [BukuApiController::class, 'store']);
Route::put('/bukus/{buku}', [BukuApiController::class, 'update']);
Route::delete('/bukus/{buku}', [BukuApiController::class, 'delete']);