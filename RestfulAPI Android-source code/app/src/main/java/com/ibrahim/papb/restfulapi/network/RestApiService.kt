package com.ibrahim.papb.restfulapi.network

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

private const val BASE_URL = "http://192.168.1.7:8080/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

/**
 * Retrofit Client
 */
private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()

/**
 * Public interface untuk service terhadap API
 */
interface RestApiService {
    /**
     * Tipe data return adalah objek model [Buku]
     * tidak menggunakan tipe data [Call]
     * karena sudah menggunakan Kotlin Coroutines
     */
    @GET("api/bukus")
    suspend fun getAllBuku() : List<Buku>

    @POST("api/bukus")
    suspend fun addBuku(@Body buku: Buku) : Buku
}

/**
 * Object API yang di-initialize secara lazy
 */
object BukuApi {
    val retrofitService : RestApiService by lazy {
        retrofit.create(RestApiService::class.java)
    }
}