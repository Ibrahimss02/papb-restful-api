package com.ibrahim.papb.restfulapi

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ibrahim.papb.restfulapi.databinding.BukuItemBinding
import com.ibrahim.papb.restfulapi.network.Buku

class BukuAdapter : ListAdapter<Buku, BukuAdapter.BukuViewHolder>(DiffCallback) {

    inner class BukuViewHolder(private val binding : BukuItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(buku : Buku) {
            binding.judulBuku.text = buku.judul
            binding.deskripsiBuku.text = buku.deskripsi
        }
    }

    /**
     * Utility untuk mengecek perubahan pada list recyclerview
     */
    companion object DiffCallback : DiffUtil.ItemCallback<Buku>() {
        override fun areItemsTheSame(oldItem: Buku, newItem: Buku): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Buku, newItem: Buku): Boolean {
            return oldItem.id == newItem.id
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BukuViewHolder {
        return BukuViewHolder(
            BukuItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: BukuViewHolder, position: Int) {
        val buku = getItem(position)
        holder.bind(buku)
    }

}