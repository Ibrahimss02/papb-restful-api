package com.ibrahim.papb.restfulapi.network

data class Buku (
    val id : Int = -1,
    val judul : String,
    val deskripsi : String
)