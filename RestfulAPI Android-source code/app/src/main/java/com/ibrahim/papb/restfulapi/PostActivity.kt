package com.ibrahim.papb.restfulapi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.ibrahim.papb.restfulapi.databinding.ActivityPostBinding
import com.ibrahim.papb.restfulapi.network.Buku
import com.ibrahim.papb.restfulapi.network.BukuApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class PostActivity : AppCompatActivity() {

    private lateinit var binding : ActivityPostBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPostBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.addButton.setOnClickListener {
            val judul = binding.etJudul.text.toString()
            val deskripsi = binding.etDeskripsi.text.toString()

            if (TextUtils.isEmpty(judul) && TextUtils.isEmpty(deskripsi)) {
                Toast.makeText(this, "Data buku tidak boleh kosong", Toast.LENGTH_LONG).show()
            } else {
                val buku = Buku(judul = judul, deskripsi = deskripsi)

                lifecycleScope.launch(Dispatchers.IO) {
                    val buku = BukuApi.retrofitService.addBuku(buku)
                    withContext(Dispatchers.Main) {
                        Toast.makeText(this@PostActivity, "${buku.judul} berhasil ditambahkan", Toast.LENGTH_LONG).show()
                        onBackPressed()
                    }
                }
            }
        }

    }
}