package com.ibrahim.papb.restfulapi

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.lifecycleScope
import com.ibrahim.papb.restfulapi.databinding.ActivityMainBinding
import com.ibrahim.papb.restfulapi.network.Buku
import com.ibrahim.papb.restfulapi.network.BukuApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainActivity : AppCompatActivity() {

    private lateinit var binding : ActivityMainBinding
    private val listBuku = MutableLiveData<List<Buku>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val adapter = BukuAdapter()
        binding.mainBukuRv.adapter = adapter

        listBuku.observe(this, {
            if (it.isNotEmpty()) {
                binding.loadingIndicator.visibility = View.INVISIBLE
                binding.mainBukuRv.visibility = View.VISIBLE
                adapter.submitList(listBuku.value)
            } else {
                binding.loadingIndicator.visibility = View.VISIBLE
                binding.mainBukuRv.visibility = View.INVISIBLE
            }
        })

        fetchBuku()

        binding.addBukuButton.setOnClickListener {
            startActivity(Intent(this, PostActivity::class.java))
            onPause()
        }
    }

    private fun fetchBuku() {
        lifecycleScope.launch(Dispatchers.IO) {
            try {
                val listBukuFetched = BukuApi.retrofitService.getAllBuku()

                withContext(Dispatchers.Main) {
                    listBuku.value = listBukuFetched
                }
            } catch (e : Exception) {
                Log.e("FetchBuku", e.message, e)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        fetchBuku()
    }
}